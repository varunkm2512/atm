import { Component, OnInit } from '@angular/core';
import { Currency } from './currency';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  currencies: Currency[] = [];
  currenciesInput: Currency[] = [];
  newCurrency: number;

  constructor() { }

  ngOnInit() {
    this.currencies = [
      { value: 2000, count: 0 },
      { value: 500, count: 0 },
      { value: 200, count: 0 },
      { value: 100, count: 0 }
    ];
    this.currenciesInput = JSON.parse(JSON.stringify(this.currencies));
  }

  insertMoney() {
    this.currenciesInput.forEach(x => {
      this.currencies.forEach(y => {
        if (x.value === y.value) {
          y.count = Number(y.count) + Number(x.count);
        }
      });
    });
    this.currenciesInput.forEach(x => {
      x.count = 0;
    });
  }

  getMoney(amount: number) {
    if (this.isValidAmount(amount)) {
      this.currencies = this.getDescendingOrder();
      this.currencies.forEach(v => {
          while (amount >= v.value && v.count) {
            amount = amount - v.value;
            v.count--;
          }
      });
    } else {
        alert('Money is not available');
    }
  }

  getDescendingOrder() {
    this.currencies.sort((a, b) => Number(b.value) - Number(a.value));
    return this.currencies;
  }

  isValidAmount(amount: number): boolean {
    if (
      amount &&
      amount <= this.getTotalAmount() &&
      amount % this.getSmallestCurrency() === 0
    ) {
      return true;
    } else {
      return false;
    }
  }

  getSmallestCurrency() {
    let smallestCurrency: number = this.currencies[0].value;
    this.currencies.forEach(c => {
      if (smallestCurrency > c.value) {
        smallestCurrency = c.value;
      }
    });
    return smallestCurrency;
  }

  getTotalAmount(): number {
    return this.currencies.reduce((a, v) => a + v.value * v.count, 0);
  }

  addNewCurrency(newCurrency: number): void {
    for (const c of this.currenciesInput) {
      if (Number(newCurrency) === c.value) {
        alert('Currency already exist.');
        return;
      }
    }
    this.currenciesInput.push({ value: Number(newCurrency), count: 0 });
    this.currencies.push({ value: Number(newCurrency), count: 0 });
  }

  removeOldCurrency(amount: number) {
    const displayCurrency: Currency[] = this.currenciesInput.filter(
      v => v.value !== Number(amount)
    );
    const actualCurrency: Currency[] = this.currencies.filter(
      v => v.value !== Number(amount)
    );
    this.currenciesInput = JSON.parse(JSON.stringify(displayCurrency));
    this.currencies = JSON.parse(JSON.stringify(displayCurrency));
  }
}
